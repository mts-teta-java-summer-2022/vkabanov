package com.mts.teta.springmvc.service;

import com.mts.teta.springmvc.dto.request.CourseRequestToUpdate;
import com.mts.teta.springmvc.dto.request.LessonRequestToCreate;
import com.mts.teta.springmvc.dto.request.LessonRequestToUpdate;
import com.mts.teta.springmvc.repository.CourseRepository;
import com.mts.teta.springmvc.repository.LessonRepository;
import com.mts.teta.springmvc.repository.UserRepository;
import com.mts.teta.springmvc.repository.model.Course;
import com.mts.teta.springmvc.repository.model.Lesson;
import com.mts.teta.springmvc.repository.model.User;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;

import static java.util.Objects.requireNonNullElse;

@Slf4j
@Service
@RequiredArgsConstructor
public class CourseServiceImpl implements CourseService {

    private final CourseRepository courseRepository;

    private final LessonRepository lessonRepository;

    private final UserRepository userRepository;

    public List<Course> getAllCourses() {
        return courseRepository.findAll();
    }

    public Course getCourseById(Long id) {
        String uid = UUID.randomUUID().toString();
        log.info("{} Start getCourseById method with param {}", uid, id);

        Course course = courseRepository.findById(id)
                .orElseThrow(() -> new NoSuchElementException("Не найдено ни одного курса по данному id"));

        log.info("{} End getCourseById method", uid);

        return course;
    }

    public void updateCourseById(Long id, CourseRequestToUpdate request) {
        String uid = UUID.randomUUID().toString();
        log.info("{} Start updateCourseById method with param {}, body: {}", uid, id, request.toString());

        Course course = courseRepository.findById(id)
                .orElseThrow(() -> new NoSuchElementException("Не найдено ни одного курса по данному id"));
        course.setTitle(request.getTitle());
        course.setAuthor(request.getAuthor());

        courseRepository.save(course);

        log.info("{} End updateCourseById method", uid);
    }

    @Override
    public void createCourse(Course course) {
        String uid = UUID.randomUUID().toString();
        log.info("{} Start createCourse method with body: {}", uid, course.toString());

        courseRepository.save(course);

        log.info("{} End createCourse method", uid);
    }

    @Override
    public void deleteCourse(Long id) {
        String uid = UUID.randomUUID().toString();
        log.info("{} Start deleteCourse method with param {}", uid, id);

        Course course = courseRepository.findById(id)
                .orElseThrow(() -> new NoSuchElementException("Не найдено ни одного курса по данному id"));
        courseRepository.deleteById(course.getId());

        log.info("{} End deleteCourse method", uid);
    }

    @Override
    public List<Course> getByTitleWithPrefix(String titlePrefix) {
        String uid = UUID.randomUUID().toString();
        log.info("{} Start getByTitleWithPrefix method with param {}", uid, titlePrefix);

        List<Course> courses = courseRepository.findByTitleStartsWith(requireNonNullElse(titlePrefix, ""));

        log.info("{} End getByTitleWithPrefix method", uid);

        return courses;
    }

    @Override
    public void createLesson(LessonRequestToCreate lessonDto, Long courseId) {
        String uid = UUID.randomUUID().toString();
        log.info("{} Start createLesson method with param {} and body: {}", uid, courseId, lessonDto.toString());

        Course course = courseRepository.findById(courseId)
                .orElseThrow(() -> new NoSuchElementException("Не найдено ни одного курса по данному id"));

        Lesson lesson = new Lesson(lessonDto.getName(), lessonDto.getText(), course);
        lessonRepository.save(lesson);

        log.info("{} End createLesson method", uid);
    }

    public void updateLessonById(Long courseId, Long lessonId, LessonRequestToUpdate request) {
        String uid = UUID.randomUUID().toString();
        log.info("{} Start updateLessonById method with params courseId: {}, lessonId: {}, body: {}",
                uid, courseId, lessonId, request.toString());

        Course course = courseRepository.findById(courseId)
                .orElseThrow(() -> new NoSuchElementException("Не найдено ни одного курса по данному id"));

        Lesson lesson = course.getLessonById(lessonId);
        lesson.setName(request.getName());
        lesson.setText(request.getText());
        lessonRepository.save(lesson);

        log.info("{} End updateLessonById method", uid);
    }

    @Override
    public void deleteLesson(Long courseId, Long lessonId) {
        String uid = UUID.randomUUID().toString();
        log.info("{} Start deleteLesson method with params courseId: {}, lessonId: {}", uid, courseId, lessonId);

        Course course = courseRepository.findById(courseId)
                .orElseThrow(() -> new NoSuchElementException("Не найдено ни одного курса по данному id"));

        Lesson lesson = course.getLessonById(lessonId);
        lessonRepository.deleteById(lesson.getId());

        course.getLessons().remove(lesson);
        courseRepository.save(course);

        log.info("{} End deleteLesson method", uid);
    }

    @Override
    public void assignUser(Long courseId, Long userId) {
        String uid = UUID.randomUUID().toString();
        log.info("{} Start assignUser method with params courseId: {}, userId: {}", uid, courseId, userId);

        User user = userRepository.findById(userId)
                .orElseThrow(() -> new NoSuchElementException("Не найдено ни одного пользователя по данному id"));
        Course course = courseRepository.findById(courseId)
                .orElseThrow(() -> new NoSuchElementException("Не найдено ни одного курса по данному id"));

        course.getUsers().add(user);
        user.getCourses().add(course);
        courseRepository.save(course);

        log.info("{} End assignUser method", uid);
    }

    @Override
    public void removeUser(Long courseId, Long userId) {
        String uid = UUID.randomUUID().toString();
        log.info("{} Start removeUser method with params courseId: {}, userId: {}", uid, courseId, userId);

        User user = userRepository.findById(userId).orElseThrow(NoSuchElementException::new);
        Course course = courseRepository.findById(courseId).orElseThrow(NoSuchElementException::new);

        course.getUsers().remove(user);
        courseRepository.save(course);

        log.info("{} End removeUser method", uid);
    }
}

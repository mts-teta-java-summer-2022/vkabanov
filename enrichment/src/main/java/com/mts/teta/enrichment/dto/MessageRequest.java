package com.mts.teta.enrichment.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class MessageRequest {

    private String content;

    private EnrichmentType enrichmentType;

    public enum EnrichmentType {
        MSISDN
    }
}

package com.mts.teta.springmvc.service;

import com.mts.teta.springmvc.dto.request.CourseRequestToUpdate;
import com.mts.teta.springmvc.dto.request.LessonRequestToCreate;
import com.mts.teta.springmvc.dto.request.LessonRequestToUpdate;
import com.mts.teta.springmvc.repository.model.Course;

import java.util.List;

public interface CourseService {

    List<Course> getAllCourses();

    Course getCourseById(Long id);

    void updateCourseById(Long id, CourseRequestToUpdate request);

    void createCourse(Course course);

    void deleteCourse(Long id);

    List<Course> getByTitleWithPrefix(String titlePrefix);

    void createLesson(LessonRequestToCreate lessonDto, Long courseId);

    void updateLessonById(Long courseId, Long lessonId, LessonRequestToUpdate request);

    void deleteLesson(Long courseId, Long lessonId);

    void assignUser(Long courseId, Long userId);

    void removeUser(Long courseId, Long userId);
}

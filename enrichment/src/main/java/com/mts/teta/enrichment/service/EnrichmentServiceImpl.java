package com.mts.teta.enrichment.service;

import com.mts.teta.enrichment.dto.MessageRequest;
import com.mts.teta.enrichment.dto.MessageResponse;
import lombok.Getter;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class EnrichmentServiceImpl implements EnrichmentService {

    private final EnrichmentFactoryImpl enrichmentFactoryImpl;

    @Getter
    private final List<String> enrichedMessages;

    @Getter
    private final List<String> notEnrichedMessages;

    public EnrichmentServiceImpl(EnrichmentFactoryImpl enrichmentFactoryImpl) {
        this.enrichmentFactoryImpl = enrichmentFactoryImpl;
        this.enrichedMessages = new CopyOnWriteArrayList<>();
        this.notEnrichedMessages = new CopyOnWriteArrayList<>();
    }

    public String enrich(MessageRequest messageRequest) {
        if (messageRequest == null) {
            return null;
        }

        if (messageRequest.getContent() != null && !messageRequest.getContent().isEmpty()) {
            EnrichmentFactory enrichmentFactory = enrichmentFactoryImpl.selectEnrichImplementation(messageRequest.getEnrichmentType());
            MessageResponse messageResponse = enrichmentFactory.enrich(messageRequest);

            if (messageResponse.isEnriched()) {
                enrichedMessages.add(messageResponse.getContent());
            } else {
                notEnrichedMessages.add(messageResponse.getContent());
            }

            return messageResponse.getContent();
        }

        return messageRequest.getContent();
    }
}

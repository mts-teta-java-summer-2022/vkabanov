package com.mts.teta.enrichment.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@AllArgsConstructor
public class MessageResponse {

    private String content;

    @Setter
    private boolean isEnriched;
}

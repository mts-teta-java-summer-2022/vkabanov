package com.mts.teta.springmvc.dto.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

@Getter
@Setter
public class LessonRequestToUpdate {

    @NotEmpty(message = "Название урока не может быть пустым")
    private String name;

    @NotEmpty(message = "Описание урока не может быть пустым")
    private String text;

    @Override
    public String toString() {
        return String.format("name - %s, text - %s", name, text);
    }
}

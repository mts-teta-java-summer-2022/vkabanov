package com.mts.teta.enrichment.service;

import com.mts.teta.enrichment.dto.MessageRequest;

public interface EnrichmentService {

    String enrich(MessageRequest messageRequest);
}

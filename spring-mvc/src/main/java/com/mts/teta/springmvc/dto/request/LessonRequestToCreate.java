package com.mts.teta.springmvc.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

@Getter
@Setter
@AllArgsConstructor
public class LessonRequestToCreate {

    @NotEmpty(message = "Название урока не может быть пустым")
    private String name;

    @NotEmpty(message = "Описание урока не может быть пустым")
    private String text;

    @Override
    public String toString() {
        return String.format("name - %s, text - %s", name, text);
    }
}

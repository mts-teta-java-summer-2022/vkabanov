package com.mts.teta.springmvc.repository.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "courses")
@AllArgsConstructor
@NoArgsConstructor
public class Course {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column
    @NotBlank(message = "Course author have to be filled")
    private String author;

    @Column
    @NotBlank(message = "Course title have to be filled")
    private String title;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "course", orphanRemoval = true, cascade = CascadeType.ALL)
    private List<Lesson> lessons;

    @ManyToMany
    private Set<User> users;

    public Course(String author, String title) {
        this.author = author;
        this.title = title;
    }

    public Lesson getLessonById(Long lessonId) {
        return lessons.stream()
                .filter(i -> lessonId.equals(i.getId()))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Не найдено ни одного урока по данному курсу"));
    }

    @Override
    public String toString() {
        return String.format("author - %s, title - %s", author, title);
    }
}

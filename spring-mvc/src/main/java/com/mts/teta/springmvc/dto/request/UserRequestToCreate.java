package com.mts.teta.springmvc.dto.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

@Getter
@Setter
public class UserRequestToCreate {

    @NotEmpty(message = "Имя пользователя не может быть пустым")
    private String username;

    @NotEmpty(message = "Пароль не может быть пустым")
    private String password;

    @Override
    public String toString() {
        return String.format("author - %s, title - %s", username, password);
    }
}

package com.mts.teta.enrichment;

import com.mts.teta.enrichment.dao.model.EnrichmentModel;
import com.mts.teta.enrichment.dao.model.MessageModel;
import com.mts.teta.enrichment.dto.MessageRequest;
import com.mts.teta.enrichment.service.EnrichmentFactoryImpl;
import com.mts.teta.enrichment.service.EnrichmentServiceImpl;
import jdk.jfr.Description;
import org.json.JSONException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static com.mts.teta.enrichment.dto.MessageRequest.EnrichmentType.MSISDN;
import static org.junit.jupiter.api.Assertions.assertEquals;

class TestEnrichment {

    EnrichmentFactoryImpl enrichmentFactory = new EnrichmentFactoryImpl();

    EnrichmentServiceImpl enrichmentServiceImpl = new EnrichmentServiceImpl(enrichmentFactory);

    @Test
    @DisplayName("Вызов без блока enrichment в запросе. Позитивный кейс")
    @Description("Проверяем, что на вызов вернулся успешный обогащенный ответ")
    void callWithoutEnrichmentAndShouldReturnWithEnrichment() throws JSONException {
        MessageModel expectedMessage = new MessageModel("88005553535", new EnrichmentModel("firstName1", "lastName1"));
        MessageModel message = new MessageModel("88005553535", null);
        MessageRequest messageRequest = new MessageRequest(message.messageToJsonString(), MSISDN);

        String result = enrichmentServiceImpl.enrich(messageRequest);
        JSONAssert.assertEquals(expectedMessage.messageToJsonString(), result, JSONCompareMode.STRICT);
    }

    @Test
    @DisplayName("Вызов с блоком enrichment в запросе. Позитивный кейс")
    @Description("Проверяем, что на вызов вернулся успешный обогащенный ответ и блок enrichment перезаписывается")
    void callWithEnrichmentAndShouldReturnWithEnrichment() throws JSONException {
        MessageModel expectedMessage = new MessageModel("88005553535", new EnrichmentModel("firstName1", "lastName1"));
        MessageModel message = new MessageModel("88005553535", new EnrichmentModel("firstNameTest", "lastNameTest"));
        MessageRequest messageRequest = new MessageRequest(message.messageToJsonString(), MSISDN);

        String result = enrichmentServiceImpl.enrich(messageRequest);
        JSONAssert.assertEquals(expectedMessage.messageToJsonString(), result, JSONCompareMode.STRICT);
    }

    @Test
    @DisplayName("Вызов в многопоточном режиме. Позитивный кейс")
    @Description("Проверяем, что списки ответов корректно заполнены в многопоточном режиме")
    void messagesShouldBeEqualsWithConcurrentCalls() throws InterruptedException {
        final int countThreads = 10;
        MessageModel enrichMessage = new MessageModel("88005553535", new EnrichmentModel("firstNameTest1", "lastNameTest1"));
        MessageRequest enrichMessageRequest = new MessageRequest(enrichMessage.messageToJsonString(), MSISDN);
        MessageModel notEnrichMessage = new MessageModel("88005553530", new EnrichmentModel("firstNameTest2", "lastNameTest2"));
        MessageRequest notEnrichMessageRequest = new MessageRequest(notEnrichMessage.messageToJsonString(), MSISDN);

        ExecutorService executorService = Executors.newFixedThreadPool(countThreads);
        CountDownLatch countDownLatch = new CountDownLatch(countThreads);

        for (int i = 0; i < countThreads; i++) {
            executorService.submit(() -> {
                enrichmentServiceImpl.enrich(enrichMessageRequest);
                enrichmentServiceImpl.enrich(notEnrichMessageRequest);
                countDownLatch.countDown();
            });
        }

        countDownLatch.await();
        executorService.shutdown();

        assertEquals(countThreads, enrichmentServiceImpl.getEnrichedMessages().size());
        assertEquals(countThreads, enrichmentServiceImpl.getNotEnrichedMessages().size());
    }

    @ParameterizedTest
    @DisplayName("Вызов с невалидным полем msisdn. Негативный кейс")
    @Description("Проверяем, что на вызов вернулось сообщение в том же виде")
    @ValueSource(strings = {"wrong_msisdn", "88005553536"})
    @NullAndEmptySource
    void shouldReturnSameForWrongMsisdn(String msisdn) throws JSONException {
        MessageModel expectedMessage = new MessageModel(msisdn, new EnrichmentModel("firstName1", "lastName1"));
        MessageModel message = new MessageModel(msisdn, new EnrichmentModel("firstName1", "lastName1"));
        MessageRequest messageRequest = new MessageRequest(message.messageToJsonString(), MSISDN);

        String result = enrichmentServiceImpl.enrich(messageRequest);
        JSONAssert.assertEquals(expectedMessage.messageToJsonString(), result, JSONCompareMode.STRICT);
    }

    @ParameterizedTest
    @DisplayName("Вызов с невалидным сообщением. Негативный кейс")
    @Description("Проверяем, что на вызов вернулось сообщение в том же виде")
    @ValueSource(strings = {"wrong_message"})
    @NullAndEmptySource
    void shouldReturnSameForWrongMessage(String message) throws JSONException {
        MessageRequest messageRequest = new MessageRequest(message, MSISDN);

        String result = enrichmentServiceImpl.enrich(messageRequest);
        JSONAssert.assertEquals(message, result, JSONCompareMode.STRICT);
    }
}
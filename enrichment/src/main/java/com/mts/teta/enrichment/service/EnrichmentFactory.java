package com.mts.teta.enrichment.service;

import com.mts.teta.enrichment.dto.MessageRequest;
import com.mts.teta.enrichment.dto.MessageResponse;

public interface EnrichmentFactory {

    MessageResponse enrich(MessageRequest messageRequest);
}

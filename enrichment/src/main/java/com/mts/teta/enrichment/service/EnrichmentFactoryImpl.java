package com.mts.teta.enrichment.service;

import com.mts.teta.enrichment.dto.MessageRequest;

public class EnrichmentFactoryImpl {

    public EnrichmentFactory selectEnrichImplementation(MessageRequest.EnrichmentType enrichmentType) {
        switch (enrichmentType) {
            case MSISDN:
                return new EnrichByMsisdnFactoryImpl();
//            case otherType:
//                return EnrichByOtherTypeFactoryImpl();
            default:
                return null;
        }
    }
}

package com.mts.teta.springmvc.mapper;

import com.mts.teta.springmvc.dto.request.CourseRequestToCreate;
import com.mts.teta.springmvc.dto.response.CourseResponse;
import com.mts.teta.springmvc.repository.model.Course;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CourseMapper {

    CourseResponse toDto(Course course);

    Course toEntity(CourseResponse courseResponse);

    Course toEntity(CourseRequestToCreate courseRequestToCreate);
}

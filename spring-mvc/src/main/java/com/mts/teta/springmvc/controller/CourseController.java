package com.mts.teta.springmvc.controller;

import com.mts.teta.springmvc.dto.request.CourseRequestToCreate;
import com.mts.teta.springmvc.dto.request.CourseRequestToUpdate;
import com.mts.teta.springmvc.dto.request.LessonRequestToCreate;
import com.mts.teta.springmvc.dto.request.LessonRequestToUpdate;
import com.mts.teta.springmvc.dto.response.CourseResponse;
import com.mts.teta.springmvc.dto.response.LessonResponse;
import com.mts.teta.springmvc.mapper.CourseMapper;
import com.mts.teta.springmvc.mapper.LessonMapper;
import com.mts.teta.springmvc.service.CourseService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("${spring.application.name}/course")
@RequiredArgsConstructor
public class CourseController {

    private final CourseService courseService;

    private final CourseMapper courseMapper;

    private final LessonMapper lessonMapper;

    @GetMapping
    public List<CourseResponse> getAllCourses(HttpSession session) {
        return courseService.getAllCourses().stream()
                .map(courseMapper::toDto)
                .collect(Collectors.toList());
    }

    @PreAuthorize("isAuthenticated()")
    @GetMapping("/{id}")
    public CourseResponse getCourse(@PathVariable("id") Long id) {
        return courseMapper.toDto(courseService.getCourseById(id));
    }

    @Secured("ROLE_ADMIN")
    @PutMapping("/{id}")
    public void updateCourse(@PathVariable Long id,
                             @RequestBody @Valid CourseRequestToUpdate request) {
        courseService.updateCourseById(id, request);
    }

    @Secured("ROLE_ADMIN")
    @PostMapping
    public void createCourse(@RequestBody @Valid CourseRequestToCreate request) {
        courseService.createCourse(courseMapper.toEntity(request));
    }

    @Secured("ROLE_ADMIN")
    @DeleteMapping("/{id}")
    public void deleteCourse(@PathVariable Long id) {
        courseService.deleteCourse(id);
    }

    @GetMapping("/filter")
    public List<CourseResponse> getCoursesByTitlePrefix(@RequestParam(name = "titlePrefix", required = false)
                                                        String titlePrefix) {
        return courseService.getByTitleWithPrefix(titlePrefix).stream()
                .map(courseMapper::toDto)
                .collect(Collectors.toList());
    }

    @Secured("ROLE_ADMIN")
    @PostMapping("/{courseId}/lesson")
    public void createLesson(@PathVariable Long courseId, @RequestBody @Valid LessonRequestToCreate request) {
        courseService.createLesson(request, courseId);
    }

    @Transactional(readOnly = true)
    @GetMapping("/{courseId}/lesson")
    public List<LessonResponse> getAllLessonsByCourseId(@PathVariable Long courseId) {
        return courseService.getCourseById(courseId).getLessons().stream()
                .map(lessonMapper::toDto)
                .collect(Collectors.toList());
    }

    @Secured("ROLE_ADMIN")
    @Transactional
    @PutMapping("/{courseId}/lesson/{lessonId}")
    public void updateLesson(@PathVariable Long courseId, @PathVariable Long lessonId,
                             @RequestBody @Valid LessonRequestToUpdate request) {
        courseService.updateLessonById(courseId, lessonId, request);
    }

    @Secured("ROLE_ADMIN")
    @Transactional
    @DeleteMapping("/{courseId}/lesson/{lessonId}")
    public void deleteLesson(@PathVariable Long courseId, @PathVariable Long lessonId) {
        courseService.deleteLesson(courseId, lessonId);
    }

    @PostMapping("/{courseId}/assign")
    public void assignUser(@PathVariable("courseId") Long courseId, @RequestParam("userId") Long userId) {
        courseService.assignUser(courseId, userId);
    }

    @DeleteMapping("/{courseId}/remove")
    public void removeUser(@PathVariable("courseId") Long courseId, @RequestParam("userId") Long userId) {
        courseService.removeUser(courseId, userId);
    }
}

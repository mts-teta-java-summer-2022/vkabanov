package com.mts.teta.springmvc.service;

import com.mts.teta.springmvc.repository.model.User;

import java.util.List;

public interface UserService {
    List<User> getAllUsers();

    void createUser(User user);

    void deleteUser(Long id);

    List<String> getUserRoles(Long userId);
}

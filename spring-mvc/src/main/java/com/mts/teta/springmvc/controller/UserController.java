package com.mts.teta.springmvc.controller;

import com.mts.teta.springmvc.dto.response.UserResponse;
import com.mts.teta.springmvc.mapper.UserMapper;
import com.mts.teta.springmvc.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("${spring.application.name}/admin/user")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    private final UserMapper userMapper;

    @GetMapping
    public List<UserResponse> getAllUsers() {
        return userService.getAllUsers().stream()
                .map(userMapper::toDto)
                .collect(Collectors.toList());
    }

    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable Long id) {
        userService.deleteUser(id);
    }

    @GetMapping("/{id}/role")
    public List<String> getUserRoles(@PathVariable Long id) {
        return userService.getUserRoles(id);
    }
}

package com.mts.teta.springmvc.service;

import com.mts.teta.springmvc.exception.UserExistsException;
import com.mts.teta.springmvc.repository.RoleRepository;
import com.mts.teta.springmvc.repository.UserRepository;
import com.mts.teta.springmvc.repository.model.Role;
import com.mts.teta.springmvc.repository.model.User;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.NoSuchElementException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

public class UserServiceTest {

    private UserService service;

    private final PasswordEncoder passwordEncoder = Mockito.mock(PasswordEncoder.class);

    private final UserRepository userRepository = Mockito.mock(UserRepository.class);

    private final RoleRepository roleRepository = Mockito.mock(RoleRepository.class);

    private User user;

    @BeforeAll
    void setUpAll() {
        user = new User();
        user.setUsername("user1");
        user.setPassword(anyString());

        service = new UserServiceImpl(userRepository, passwordEncoder, roleRepository);
    }

    @Test
    @DisplayName("Позитивный тест. Проверка сохранения нового пользователя")
    public void successCreateUserNewUserShouldBeSaved() {
        User user = new User();
        user.setUsername("user2");
        user.setPassword(anyString());

        when(roleRepository.findByName("ROLE_STUDENT")).thenReturn(Optional.of(new Role()));
        when(userRepository.findByUsername(user.getUsername())).thenReturn(Optional.empty());

        service.createUser(user);
        verify(passwordEncoder, times(1)).encode(anyString());
        verify(userRepository, times(1)).findByUsername(user.getUsername());
        verify(roleRepository, times(1)).findByName(anyString());
        verify(userRepository, times(1)).save(user);
    }

    @Test
    @DisplayName("Негативный тест. Попытка сохранения уже существующего пользователя")
    public void failureCreateUserWithUserAlreadyExists() {
        when(userRepository.findByUsername(user.getUsername())).thenReturn(Optional.of(user));
        assertThrows(UserExistsException.class, () -> service.createUser(user));
        verify(passwordEncoder, times(0)).encode(anyString());
        verify(roleRepository, times(0)).findByName(anyString());
        verify(userRepository, times(0)).save(user);
    }

    @Test
    @DisplayName("Негативный тест. Попытка сохранения пользователя без роли")
    public void failureCreateUserSaveUserWithoutRole() {
        when(userRepository.findByUsername(user.getUsername())).thenReturn(Optional.empty());
        when(roleRepository.findByName("ROLE_STUDENT")).thenReturn(Optional.empty());
        assertThrows(NoSuchElementException.class, () -> service.createUser(user));
    }
}

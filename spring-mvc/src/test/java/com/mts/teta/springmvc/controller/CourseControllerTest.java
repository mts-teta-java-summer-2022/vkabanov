package com.mts.teta.springmvc.controller;

import com.mts.teta.springmvc.mapper.CourseMapper;
import com.mts.teta.springmvc.mapper.LessonMapper;
import com.mts.teta.springmvc.service.CourseServiceImpl;
import com.mts.teta.springmvc.service.UserAuthServiceImpl;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(CourseController.class)
@ActiveProfiles("test")
public class CourseControllerTest {

    @Value("${spring.application.name}")
    private String applicationName;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CourseServiceImpl courseService;

    @MockBean
    private UserAuthServiceImpl userAuthService;

    @MockBean
    private CourseMapper courseMapper;

    @MockBean
    private LessonMapper lessonMapper;

    @MockBean
    private PasswordEncoder passwordEncoder;

    @Test
    @DisplayName("Позитивный тест. Вызов метода GET /course")
    void successGetAllCourses() throws Exception {
        mockMvc.perform(get(applicationName + "/course")
                        .contentType(APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is2xxSuccessful());

        verify(courseService, times(1)).getAllCourses();
    }

    @Test
    @DisplayName("Позитивный тест. Вызов метода GET /course/{id}")
    void successGetCourseById() throws Exception {
        mockMvc.perform(get(applicationName + "/course/1")
                        .contentType(APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is3xxRedirection());

        verify(courseService, times(0)).getCourseById(anyLong());
    }
}

package com.mts.teta.enrichment.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mts.teta.enrichment.dao.UserStorage;
import com.mts.teta.enrichment.dao.model.EnrichmentModel;
import com.mts.teta.enrichment.dto.MessageRequest;
import com.mts.teta.enrichment.dto.MessageResponse;

import java.util.Iterator;
import java.util.Optional;

public class EnrichByMsisdnFactoryImpl implements EnrichmentFactory {

    private final UserStorage userStorage;

    public EnrichByMsisdnFactoryImpl() {
        this.userStorage = new UserStorage();
    }

    @Override
    public MessageResponse enrich(MessageRequest messageRequest) {
        try {
            ObjectNode objectNode = new ObjectMapper().readValue(messageRequest.getContent(), ObjectNode.class);
            Iterator<String> fieldNames = objectNode.fieldNames();

            while (fieldNames.hasNext()) {
                String fieldName = fieldNames.next();

                if ("msisdn".equals(fieldName) && !objectNode.get(fieldName).asText().isEmpty()) {
                    String msisdnValue = objectNode.get(fieldName).asText();
                    Optional<EnrichmentModel> enrichmentOptional = userStorage.getEnrichmentByMsisdn(msisdnValue);

                    if (enrichmentOptional.isPresent()) {
                        objectNode.putPOJO("enrichment", enrichmentOptional.get());

                        return new MessageResponse(objectNode.toPrettyString(), true);
                    }
                }
            }
        } catch (JsonProcessingException e) {
            return new MessageResponse(messageRequest.getContent(), false);
        }

        return new MessageResponse(messageRequest.getContent(), false);
    }
}

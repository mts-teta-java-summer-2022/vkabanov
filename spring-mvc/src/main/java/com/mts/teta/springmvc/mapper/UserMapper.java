package com.mts.teta.springmvc.mapper;

import com.mts.teta.springmvc.dto.request.UserRequestToCreate;
import com.mts.teta.springmvc.dto.response.UserResponse;
import com.mts.teta.springmvc.repository.model.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface UserMapper {

    UserResponse toDto(User user);

    @Mapping(target = "active", constant = "true")
    User toEntity(UserRequestToCreate request);
}

package com.mts.teta.springmvc.service;

import com.mts.teta.springmvc.dto.request.LessonRequestToCreate;
import com.mts.teta.springmvc.repository.CourseRepository;
import com.mts.teta.springmvc.repository.model.Course;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;
import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("testcontainers")
class CourseServiceTest {

    @Autowired
    private CourseService courseService;

    @Autowired
    private CourseRepository courseRepository;

    @BeforeEach
    void cleanEach() {
        courseRepository.deleteAll();
    }

    @Test
    @DisplayName("Позитивный тест. Проверка создания курса")
    void successCreateCourseNewCourseShouldBeSaved() {
        Course course = new Course("author", "title");
        courseService.createCourse(course);

        List<Course> courses = courseRepository.findByAuthor(course.getAuthor());

        assertEquals(1, courseRepository.count());
        courses.forEach(i -> {
            assertNotNull(i.getId());
            assertEquals("author", i.getAuthor());
            assertEquals("title", i.getTitle());
        });
    }

    @Test
    @DisplayName("Негативный тест. Попытка создать урок к несуществующему курсу")
    void failureCreateLessonWithNotFoundCourse() {
        LessonRequestToCreate lesson = new LessonRequestToCreate("name", "text");
        assertThrows(NoSuchElementException.class, () -> courseService.createLesson(lesson, 1L));
    }
}
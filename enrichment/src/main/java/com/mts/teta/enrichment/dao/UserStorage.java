package com.mts.teta.enrichment.dao;

import com.mts.teta.enrichment.dao.model.EnrichmentModel;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class UserStorage {

    private final Map<String, EnrichmentModel> users = new HashMap<>() {{
        put("88005553535", new EnrichmentModel("firstName1", "lastName1"));
        put("88005553534", new EnrichmentModel("firstName2", "lastName2"));
        put("88005553533", new EnrichmentModel("firstName3", "lastName3"));
        put("88005553532", new EnrichmentModel("firstName4", "lastName4"));
        put("88005553531", new EnrichmentModel("firstName5", "lastName5"));
    }};

    public Optional<EnrichmentModel> getEnrichmentByMsisdn(String msisdn) {
        return Optional.ofNullable(users.get(msisdn));
    }
}

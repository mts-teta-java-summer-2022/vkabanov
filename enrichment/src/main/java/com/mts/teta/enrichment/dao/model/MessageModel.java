package com.mts.teta.enrichment.dao.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;

@Getter
public class MessageModel {

    private final String action;

    private final String page;

    private String msisdn;

    private EnrichmentModel enrichment;

    public MessageModel(String msisdn, EnrichmentModel enrichment) {
        this.action = "actionValue";
        this.page = "pageValue";
        this.msisdn = msisdn;
        this.enrichment = enrichment;
    }

    public String messageToJsonString() {
        try {
            return new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(this);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}

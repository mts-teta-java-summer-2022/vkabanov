package com.mts.teta.springmvc.repository.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "lessons")
@AllArgsConstructor
@NoArgsConstructor
public class Lesson {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @Lob
    @Column
    private String text;

    @ManyToOne(optional = false)
    @JoinColumn(name = "course_id")
    private Course course;

    public Lesson(String name, String text, Course course) {
        this.name = name;
        this.text = text;
        this.course = course;
    }
}

package com.mts.teta.springmvc.config;

import com.mts.teta.springmvc.dto.CourseError;
import com.mts.teta.springmvc.exception.UserExistsException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.time.OffsetDateTime;
import java.util.NoSuchElementException;

import static java.time.format.DateTimeFormatter.ISO_ZONED_DATE_TIME;

@Slf4j
@RestControllerAdvice
public class CourseExceptionHandler {

    @ExceptionHandler(NoSuchElementException.class)
    public ResponseEntity<CourseError> handleNoSuchElementException(NoSuchElementException ex) {
        logError(ex);
        String dateOccurred = OffsetDateTime.now().format(ISO_ZONED_DATE_TIME);
        CourseError courseError = new CourseError("Элемент не найден", ex.getMessage(), dateOccurred);

        return new ResponseEntity<>(courseError, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<CourseError> handleMethodArgumentNotValidException(MethodArgumentNotValidException ex) {
        logError(ex);
        CourseError courseError = new CourseError("Ошибка валидации", ex.getFieldError().getDefaultMessage());

        return new ResponseEntity<>(courseError, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(UserExistsException.class)
    public ResponseEntity<CourseError> handleUserExistsException(UserExistsException ex) {
        logError(ex);
        CourseError courseError = new CourseError("Пользователь уже существует", ex.getMessage());

        return new ResponseEntity<>(courseError, HttpStatus.BAD_REQUEST);
    }

    private void logError(Exception ex) {
        log.error("[Got exception {}]. Cause: {}.", ex.getClass().getSimpleName(), ex.getCause());
    }
}

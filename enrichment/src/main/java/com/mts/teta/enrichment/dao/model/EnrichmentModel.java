package com.mts.teta.enrichment.dao.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@AllArgsConstructor
public class EnrichmentModel {

    private String firstName;

    private String lastName;
}

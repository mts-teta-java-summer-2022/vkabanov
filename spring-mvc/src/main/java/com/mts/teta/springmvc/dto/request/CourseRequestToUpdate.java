package com.mts.teta.springmvc.dto.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

@Getter
@Setter
public class CourseRequestToUpdate {

    @NotEmpty(message = "Автор не может быть пустым")
    private String author;

    @NotEmpty(message = "Название курса не может быть пустым")
    private String title;

    @Override
    public String toString() {
        return String.format("author - %s, title - %s", author, title);
    }
}

package com.mts.teta.springmvc.mapper;

import com.mts.teta.springmvc.dto.response.LessonResponse;
import com.mts.teta.springmvc.repository.model.Lesson;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface LessonMapper {

    LessonResponse toDto(Lesson lesson);
}

-- Количество пользователей, которые не создали ни одного поста.
SELECT COUNT(p1)
FROM profile p1
         LEFT JOIN post p2 ON p1.profile_id = p2.profile_id
WHERE p2.profile_id IS NULL;
-- count = 5
-- 1 row retrieved starting from 1 in 24 ms (execution: 6 ms, fetching: 18 ms)

-- Выберите ID всех постов по возрастанию, у которых 2 комментария,
-- title начинается с цифры, а длина content больше 20.
SELECT p.post_id
FROM post p
         JOIN comment c ON p.post_id = c.post_id
WHERE p.title ~ '^[0-9]'
  AND length(p.content) > 20
GROUP BY p.post_id
HAVING COUNT(c.comment_id) = 2
ORDER BY p.post_id;
-- post_id
-- 22
-- 24
-- 26
-- 28
-- 32
-- 34
-- 36
-- 38
-- 42
-- 44
-- 10 rows retrieved starting from 1 in 12 ms (execution: 5 ms, fetching: 7 ms)

-- Выберите первые 3 ID постов по возрастанию,
-- у которых либо нет комментариев, либо он один.
SELECT p.post_id
FROM post p
         JOIN comment c ON p.post_id = c.post_id
GROUP BY p.post_id
HAVING COUNT(c.post_id) < 2
ORDER BY p.post_id
    LIMIT 3;
-- post_id
-- 1
-- 3
-- 5
-- 3 rows retrieved starting from 1 in 23 ms (execution: 5 ms, fetching: 18 ms)
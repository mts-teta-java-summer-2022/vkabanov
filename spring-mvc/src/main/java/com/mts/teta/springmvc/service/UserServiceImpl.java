package com.mts.teta.springmvc.service;

import com.mts.teta.springmvc.exception.UserExistsException;
import com.mts.teta.springmvc.repository.RoleRepository;
import com.mts.teta.springmvc.repository.UserRepository;
import com.mts.teta.springmvc.repository.model.Role;
import com.mts.teta.springmvc.repository.model.User;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    private final RoleRepository roleRepository;

    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public void createUser(User user) {
        String uid = UUID.randomUUID().toString();
        log.info("{} Start createUser method with body: {}", uid, user.toString());
        Optional<User> userFromDb = userRepository.findByUsername(user.getUsername());

        if (userFromDb.isPresent()) {
            throw new UserExistsException(String.format("Пользователь с username = '%s' уже существует",
                    user.getUsername()));
        }

        Role role = roleRepository.findByName("ROLE_STUDENT")
                .orElseThrow(() -> new NoSuchElementException("Роль не найдена"));
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.getRoles().add(role);
        userRepository.save(user);

        log.info("{} End createUser method", uid);
    }

    @Override
    public void deleteUser(Long id) {
        String uid = UUID.randomUUID().toString();
        log.info("{} Start deleteUser method with param {}", uid, id);

        User user = userRepository.findById(id)
                .orElseThrow(() -> new NoSuchElementException("Не найдено ни одного пользователя по данному id"));
        userRepository.deleteById(user.getId());

        log.info("{} End deleteUser method", uid);
    }

    @Override
    public List<String> getUserRoles(Long userId) {
        String uid = UUID.randomUUID().toString();
        log.info("{} Start getUserRoles method with param {}", uid, userId);
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new NoSuchElementException("Не найдено ни одного пользователя по данному id"));

        List<String> roles = user.getRoles().stream()
                .map(Role::getName)
                .collect(Collectors.toList());

        log.info("{} End getUserRoles method", uid);

        return roles;
    }
}

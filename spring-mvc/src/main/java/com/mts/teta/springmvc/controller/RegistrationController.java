package com.mts.teta.springmvc.controller;

import com.mts.teta.springmvc.dto.request.UserRequestToCreate;
import com.mts.teta.springmvc.mapper.UserMapper;
import com.mts.teta.springmvc.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RequiredArgsConstructor
@RestController
@RequestMapping("${spring.application.name}/registration")
public class RegistrationController {

    private final UserService userService;

    private final UserMapper userMapper;

    @PostMapping
    public void createUser(@RequestBody @Valid UserRequestToCreate request) {
        userService.createUser(userMapper.toEntity(request));
    }
}

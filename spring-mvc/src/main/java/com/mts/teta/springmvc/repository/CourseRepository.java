package com.mts.teta.springmvc.repository;

import com.mts.teta.springmvc.repository.model.Course;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CourseRepository extends JpaRepository<Course, Long> {
    List<Course> findByTitleStartsWith(String titlePrefix);

    List<Course> findByAuthor(String author);
}
